package com.kanzi.musicbrainz.ui.map

import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.Marker
import com.kanzi.musicbrainz.api.AppService
import com.kanzi.musicbrainz.api.model.Place
import com.kanzi.musicbrainz.utils.Constant
import com.kanzi.musicbrainz.utils.extensions.convertDateToInt
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MapViewModel @Inject constructor(
    private val appService: AppService
) : ViewModel() {

    data class MapState(
        val isLoading: Boolean,
        val places: List<Place>
    )

    private val expiredPin = BehaviorSubject.create<Marker>()

    private val mapState = BehaviorSubject.createDefault(
        MapState(
            isLoading = false,
            places = emptyList()
        )
    )

    private val disposable = CompositeDisposable()

    fun fetchPlaces(
        query: String
    ) {
        appService
            .searchRelease(query)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                mapState.onNext(
                    mapState.value!!.copy(
                        isLoading = true,
                        places = emptyList()
                    )
                )
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    mapState.onNext(
                        mapState.value!!.copy(
                            isLoading = false,
                            places = filterPlaces(response.places)
                        )
                    )
                },
                { error ->
                    mapState.onNext(
                        mapState.value!!.copy(
                            isLoading = false
                        )
                    )
                }
            )
            .addTo(disposable)
    }

    private fun filterPlaces(places: List<Place>): List<Place> {
        return places.filter {
            it.lifeSpan?.begin != null &&
                    it.lifeSpan.begin.convertDateToInt() > Constant.MIN_YEAR
        }
    }

    fun initMarkerTimer(openFrom: Long, marker: Marker) {
        val countDownTime = Constant.CURRENT_YEAR - openFrom
        getTimer(countDownTime)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {},
                {},
                {
                    expiredPin.onNext(marker)
                }
            )
            .addTo(disposable)
    }

    private fun getTimer(time: Long): Observable<Long> {
        return Observable
            .interval(1, TimeUnit.SECONDS)
            .take(time)
    }

    fun getMapState(): Observable<MapState> {
        return mapState
    }

    fun getExpiredPin(): Observable<Marker> {
        return expiredPin
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}