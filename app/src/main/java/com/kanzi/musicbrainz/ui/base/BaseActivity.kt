package com.kanzi.musicbrainz.ui.base

import android.content.Context
import android.graphics.PixelFormat
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.kanzi.musicbrainz.utils.LoadingListener
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity(), LoadingListener {

    override fun showLoading() {
        runOnUiThread {
            try {
                val layoutParams =
                    WindowManager.LayoutParams(WindowManager.LayoutParams.FIRST_SUB_WINDOW)
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT

                layoutParams.format = PixelFormat.RGBA_8888
                layoutParams.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN or
                        WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
                layoutParams.token = window.decorView.rootView.windowToken

                (getSystemService(Context.WINDOW_SERVICE) as WindowManager).addView(
                    progressOverlay,
                    layoutParams
                )
            } catch (ignore: Exception) {
            }
        }
    }

    override fun hideLoading() {
        runOnUiThread {
            try {
                (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(
                    progressOverlay
                )
            } catch (ignore: Exception) {
            }
        }
    }

    private val progressOverlay by lazy {
        val fl = FrameLayout(this)
        val whiteBox = FrameLayout(this)
        val progressBar = ProgressBar(this)
        val lp = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        lp.gravity = Gravity.CENTER
        fl.addView(whiteBox, lp)
        whiteBox.addView(progressBar, lp)
        fl
    }
}