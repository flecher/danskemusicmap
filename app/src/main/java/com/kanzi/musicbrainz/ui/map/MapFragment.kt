package com.kanzi.musicbrainz.ui.map

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.kanzi.musicbrainz.R
import com.kanzi.musicbrainz.api.model.Place
import com.kanzi.musicbrainz.ui.base.BaseFragment
import com.kanzi.musicbrainz.utils.Constant
import com.kanzi.musicbrainz.utils.extensions.convertDateToInt
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class MapFragment : BaseFragment(), OnMapReadyCallback {

    val TAG = MapFragment::class.java.simpleName

    override var layoutId: Int = R.layout.fragment_map

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mapViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
            .get(MapViewModel::class.java)
    }

    private val disposable by lazy { CompositeDisposable() }

    private lateinit var mMap: GoogleMap

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val map = childFragmentManager.findFragmentById(R.id.googleMap) as SupportMapFragment
        map.getMapAsync(this)

        observeMapViewModel()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    private fun observeMapViewModel() {
        mapViewModel
            .getMapState()
            .subscribe(
                { state ->
                    showLoading(state.isLoading)
                    if (state.places.isNotEmpty()) {
                        addMarker(state.places)
                    }
                },
                { error ->
                    Log.e(TAG, "Failure", error)
                }
            )
            .addTo(disposable)
        mapViewModel
            .getExpiredPin()
            .subscribe {
                removeMarker(it)
            }
            .addTo(disposable)
    }

    private fun addMarker(places: List<Place>) {
        mMap.clear()
        places.forEach { place ->
            place.coordinates?.let {
                val latLng = LatLng(it.latitude, it.longitude)
                val marker = mMap
                    .addMarker(
                        MarkerOptions()
                            .position(latLng)
                            .title(place.name)
                    )
                mMap
                    .moveCamera(
                        CameraUpdateFactory
                            .newLatLngZoom(latLng, Constant.MAP_ZOOM_LEVEL)
                    )
                mapViewModel
                    .initMarkerTimer(
                        place.lifeSpan!!.begin!!.convertDateToInt().toLong(),
                        marker
                    )
            }
        }
    }

    private fun removeMarker(marker: Marker) {
        marker.remove()
    }

    override fun onMapReady(mMap: GoogleMap) {
        this.mMap = mMap
    }
}