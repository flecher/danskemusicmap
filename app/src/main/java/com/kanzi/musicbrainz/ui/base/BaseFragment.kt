package com.kanzi.musicbrainz.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.kanzi.musicbrainz.utils.LoadingListener
import dagger.android.support.DaggerFragment


abstract class BaseFragment : DaggerFragment() {

    @get:LayoutRes
    abstract var layoutId: Int

    private val baseActivity: BaseActivity
        get() = activity as BaseActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId, container, false)
    }

    fun showLoading(show: Boolean) {
        if (show) {
            (baseActivity as LoadingListener).showLoading()
        } else {
            (baseActivity as LoadingListener).hideLoading()
        }
    }
}