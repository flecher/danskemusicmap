package com.kanzi.musicbrainz.api.model

data class LifeSpan(
    val begin: String?,
    val ended: String?
)