package com.kanzi.musicbrainz.api

import com.kanzi.musicbrainz.api.model.PlacesSearchResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface AppService {

    @GET("place")
    fun searchRelease(
        @Query("query") searchText: String,
        @Query("fmt") responseFormat: String = "json"
    ): Observable<PlacesSearchResponse>
}