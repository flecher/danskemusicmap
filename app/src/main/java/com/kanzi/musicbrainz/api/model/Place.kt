package com.kanzi.musicbrainz.api.model

import com.squareup.moshi.Json

data class Place (
    val name: String,
    val coordinates: Coordinates?,
    @Json(name = "life-span")
    val lifeSpan: LifeSpan?
)