package com.kanzi.musicbrainz.api.model

data class PlacesSearchResponse(
    val count: Int,
    val offset: Int,
    val places: List<Place>
)