package com.kanzi.musicbrainz.api.model

data class Coordinates (
    val latitude: Double,
    val longitude: Double
)