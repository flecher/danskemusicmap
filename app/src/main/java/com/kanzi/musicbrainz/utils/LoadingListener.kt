package com.kanzi.musicbrainz.utils

interface LoadingListener {
    fun showLoading()

    fun hideLoading()
}