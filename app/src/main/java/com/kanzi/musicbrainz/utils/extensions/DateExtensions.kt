package com.kanzi.musicbrainz.utils.extensions

fun String.convertDateToInt(): Int {
    return if (length > 4) {
        substring(0, 4).toInt()
    } else {
        toInt()
    }
}