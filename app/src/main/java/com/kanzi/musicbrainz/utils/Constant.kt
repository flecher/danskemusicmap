package com.kanzi.musicbrainz.utils

object Constant {
    const val MAP_ZOOM_LEVEL = 12.0f
    const val MIN_YEAR = 1990
    const val CURRENT_YEAR = 2019
}