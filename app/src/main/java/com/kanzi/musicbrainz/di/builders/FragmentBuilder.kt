package com.kanzi.musicbrainz.di.builders

import com.kanzi.musicbrainz.ui.map.MapFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract fun contributeMapFramgnet(): MapFragment
}