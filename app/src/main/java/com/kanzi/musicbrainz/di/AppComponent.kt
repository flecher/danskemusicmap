package com.kanzi.musicbrainz.di

import com.kanzi.musicbrainz.DanskeMusicApp
import com.kanzi.musicbrainz.di.builders.ActivityBuilder
import com.kanzi.musicbrainz.di.modules.ApiModule
import com.kanzi.musicbrainz.di.modules.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ActivityBuilder::class,
        ApiModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AndroidInjector<DanskeMusicApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<DanskeMusicApp>()
}